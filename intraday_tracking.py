from config import *
import psycopg2.extras
from datetime import datetime
import util
import csv
import os
import queries


def create_connection():
    print 'Connecting to database.'
    conn = None
    try:
        conn = psycopg2.connect("dbname='%s' port='%s' user='%s' host='%s' password='%s'" % (
        DB_NAME, PORT, MASTER_USERNAME, HOST, MASTER_PASSWORD))
        print 'Connected successfully.'
    except Exception as e:
        print "Unable to execute query."
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    return cur


def get_brand_name(brand):
    if brand == "Moda Rapido":
        return "('Moda Rapido', 'Moda Rapido Disney', 'Moda Rapido Marvel', 'Moda Rapido Star Wars')"
    else:
        return "('" + brand + "')"


def get_metrics(cur, brand, article_type, gender, current_hour, previous_hour, current_date):
    key_mappings = {
        '[START_DATE]': current_date,
        '[HOUR]': current_hour,
        '[ARTICLE_TYPE]': article_type,
        '[GENDER]': gender,
        '[BRAND]': get_brand_name(brand)
    }
    current_hour_query_string = util.build_query(queries.INTRA_DAY_TREND_TRACKING_QUERY, key_mappings)
    key_mappings = {
        '[START_DATE]': current_date,
        '[HOUR]': previous_hour,
        '[ARTICLE_TYPE]': article_type,
        '[GENDER]': gender,
        '[BRAND]': get_brand_name(brand)
    }
    previous_hour_query_string = util.build_query(queries.INTRA_DAY_TREND_TRACKING_QUERY, key_mappings)
    previous_hour_row = None
    current_hour_row = None
    try:
        cur.execute(previous_hour_query_string)
        previous_hour_row = cur.fetchone()
        cur.execute(current_hour_query_string)
        current_hour_row = cur.fetchone()
        return previous_hour_row, current_hour_row
    except Exception as e:
        print "Unable to execute query."
    return previous_hour_row, current_hour_row


def calculate_delta(previous_hour, current_hour):
    diff = current_hour['roi'] - previous_hour['roi']
    if diff / previous_hour['roi'] < -0.03 or diff / previous_hour['roi'] > 0.03:
        delta = diff / abs(diff)
    else:
        delta = 0
    return delta


def update_tracker(current_hour, trend_map):
    if current_hour == '200' or os.path.exists(TRACKER_FILE_PATH) is False:
        for k, v in trend_map.iteritems():
            if v > 0:
                trend_map[k] = 0
        write_to_tracker(trend_map)
    else:
        tracker_map = read_tracker()
        final_map = dict()
        if tracker_map is not None:
            for k, v in trend_map.iteritems():
                if tracker_map[k] < 0:
                    if v == 0:
                        final_map[k] = 0
                    else:
                        final_map[k] = tracker_map[k] + v
                elif tracker_map[k] == 0 and v < 0:
                    final_map[k] = tracker_map[k] + v
                else:
                    final_map[k] = 0
            write_to_tracker(final_map)
        else:
            for k, v in trend_map.iteritems():
                if v > 0:
                    trend_map[k] = 0
            write_to_tracker(trend_map)


def write_to_tracker(trend_map):
    with open(TRACKER_FILE_PATH, 'w') as f:
        f.write('category,trend\n')
        for k, v in trend_map.iteritems():
            f.write(k + ',' + str(v) + '\n')


def read_tracker():
    if os.path.exists(TRACKER_FILE_PATH) is False:
        return None
    else:
        tracker_map = dict()
        with open(TRACKER_FILE_PATH, 'r') as f:
            csv_dict = csv.DictReader(f)
            for obj in csv_dict:
                tracker_map[obj['category']] = int(obj['trend'])
        return tracker_map


def track_metrics():
    trend_map = dict()
    current_date = datetime.today().strftime('%Y%m%d')
    current_hour = str(datetime.now().hour) + '00'
    previous_hour = str(datetime.now().hour - 1) + '00'
    cursor = create_connection()
    with open(CATEGORIES_FILE_PATH, 'r') as f:
        csv_dict = csv.DictReader(f)
        for obj in csv_dict:
            print "Querying for " + obj['brand'] + " " + obj['article_type'] + " " + obj['gender']
            previous_hour_row, current_hour_row = get_metrics(cursor, obj['brand'], obj['article_type'], obj['gender'], current_hour,
                                                              previous_hour, current_date)
            trend_map[obj['brand'] + '_' + obj['article_type'] + '_' + obj['gender']] = calculate_delta(previous_hour_row, current_hour_row)
    update_tracker(current_hour, trend_map)


track_metrics()
