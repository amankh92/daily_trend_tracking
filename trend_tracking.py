import psycopg2
import psycopg2.extras
import util
from config import *
from datetime import datetime, timedelta
import csv
import queries


def connect_to_db():
    print 'Connecting to DB.'
    conn = None
    try:
        conn = psycopg2.connect("dbname='%s' port='%s' user='%s' host='%s' password='%s'" % (
            DB_NAME, PORT, MASTER_USERNAME, HOST, MASTER_PASSWORD))
        print 'Connected successfully.'
    except Exception as e:
        print "Unable to execute query."
    return conn


def get_brand_name(brand):
    if brand == "Moda Rapido":
        return "('Moda Rapido', 'Moda Rapido Disney', 'Moda Rapido Marvel', 'Moda Rapido Star Wars')"
    else:
        return "('" + brand + "')"


def generate_filename(brand, article_type, gender):
    return brand.replace(" ", "").lower() + "_" + article_type.replace(" ", "").lower() + "_" + gender.lower() + "_" + METRICS_MEANS_FILENAME


def get_current_metrics(current_hour, brand, article_type, gender):
    conn = connect_to_db()
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    current_date = datetime.today().strftime('%Y%m%d')
    key_mappings = {
        '[START_DATE]': current_date,
        '[END_DATE]': current_date,
        '[HOUR]': current_hour,
        '[ARTICLE_TYPE]': article_type,
        '[GENDER]': gender,
        '[BRAND]': get_brand_name(brand)
    }
    query_string = util.build_query(queries.INTRA_DAY_TREND_TRACKING_QUERY, key_mappings)
    try:
        cur.execute(query_string)
    except Exception as e:
        print "Unable to execute query."
    return cur.fetchone()


def get_baseline_metrics(brand, article_type, gender):
    conn = connect_to_db()
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    dt = datetime.today()
    base_date = str((dt - timedelta(days=dt.weekday())).strftime('%Y%m%d'))
    key_mappings = {
        '[START_DATE]': base_date,
        '[ARTICLE_TYPE]': article_type,
        '[GENDER]': gender,
        '[BRAND]': get_brand_name(brand)
    }
    query_string = util.build_query(queries.METRICS_MOST_RECENT_BASELINE, key_mappings)
    print query_string
    try:
        cur.execute(query_string)
    except Exception as e:
        print "Unable to execute query."
    return cur.fetchone()


def get_metrics_means(brand, article_type, gender):
    means_map = dict()
    with open(generate_filename(brand, article_type, gender), 'r') as f:
        csv_obj = csv.DictReader(f)
        for obj in csv_obj:
            means_map[obj['index']] = obj
    return means_map


def get_time_suffix():
    current_time = datetime.now().hour
    if current_time <= 4:
        return '_04am'
    elif current_time <= 8:
        return '_08am'
    elif current_time <= 12:
        return '_12pm'
    elif current_time <= 14:
        return '_02pm'
    elif current_time <= 16:
        return '_04pm'
    elif current_time <= 18:
        return '_06pm'
    elif current_time <= 20:
        return '_08pm'
    elif current_time <= 23:
        return '_11pm'


def reinforce_metrics(baseline_metrics, historic_metrics):
    final_metrics = dict()
    for key, value in historic_metrics.iteritems():
        final_metrics[key] = dict()
        final_metrics[key]['mean'] = (1 - BASELINE_WEIGHT) * float(value['mean']) + BASELINE_WEIGHT * float(baseline_metrics[key])
        final_metrics[key]['sd'] = (1 - BASELINE_WEIGHT) * float(value['sd']) + BASELINE_WEIGHT * float(baseline_metrics[key])
    return final_metrics


def take_action(brand, article_type, gender):
    print brand, article_type, gender
    current_hour = str(datetime.now().hour) + '00'
    actions = {
        'rev': 0,
        'rgm': 0,
        'disc': 0
    }
    baseline = get_baseline_metrics(brand, article_type, gender)
    print baseline
    means_map = get_metrics_means(brand, article_type, gender)
    final_metrics_map = reinforce_metrics(baseline, means_map)
    print final_metrics_map
    row = get_current_metrics(current_hour, brand, article_type, gender)
    print row
    time_suffix = get_time_suffix()
    if row['roi'] < float(final_metrics_map['roi' + time_suffix]['mean']) - (ROI_FACTOR * float(final_metrics_map['roi' + time_suffix]['sd'])):
        rev_lower_limit = float(final_metrics_map['rev_c_perc' + time_suffix]['mean']) - (REV_FACTOR * float(final_metrics_map['rev_c_perc' + time_suffix]['sd']))
        if row['rev_c_perc'] < rev_lower_limit:
            actions['rev'] = 1
        rgm_lower_limit = float(final_metrics_map['rgm_c_perc' + time_suffix]['mean']) - (RGM_FACTOR * float(final_metrics_map['rgm_c_perc' + time_suffix]['sd']))
        if row['rgm_c_perc'] < rgm_lower_limit:
            actions['rgm'] = 1
        disc_upper_limit = float(final_metrics_map['disc_c_perc' + time_suffix]['mean']) + (DISC_FACTOR * float(final_metrics_map['disc_c_perc' + time_suffix]['sd']))
        if disc_upper_limit < row['disc_c_perc']:
            actions['disc'] = 1
        return actions
    else:
        return {}


if __name__ == "__main__":
    current_date = datetime.today().strftime('%Y%m%d')
    current_hour = str(datetime.now().hour) + '00'
    with open('actions_' + current_date + '_' + current_hour + ".csv", 'w') as f:
        with open(CATEGORIES_FILE_PATH, 'r') as cat_file:
            csv_dict = csv.DictReader(cat_file)
            for obj in csv_dict:
                action_dict = take_action(obj['brand'], obj['article_type'], obj['gender'])
                if len(action_dict) == 0:
                    f.write(obj['brand'] + '_' + obj['article_type'] + '_' + obj['gender'] + ',' + "Take no action!\n")
                else:
                    for k, v in action_dict.iteritems():
                        if v == 1:
                            f.write(obj['brand'] + '_' + obj['article_type'] + '_' + obj[
                                'gender'] + ',' + "Take" + k + "action!\n")
                break
