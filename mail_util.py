import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import Encoders


class MailUtil:
    def __init__(self, from_addr, from_password, to_addr, subject, email_body, attachment="", attachment_name=""):
        self.from_addr = from_addr
        self.from_password = from_password
        self.to_addr = to_addr
        self.subject = subject
        self.email_body = email_body
        self.attachment = attachment
        self.attachment_name = attachment_name
        pass

    def send(self):
        server = None
        try:
            server = self._create_server()
            msg = self._create_message()
            msg = self._attach(msg)
            self._send(msg, server)
        finally:
            if server is not None:
                server.quit()

    def send_as_html(self, json):
        # TODO Naman
        # This method will put json as a html after the email body ina  tabular format
        pass

    def _create_server(self):
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(self.from_addr, self.from_password)
        return server

    def _create_message(self):
        msg = MIMEMultipart()
        msg['From'] = self.from_addr
        if isinstance(self.to_addr, list):
            msg['To'] = ','.join(self.to_addr)
        else:
            msg['To'] = self.to_addr
        msg['Subject'] = self.subject
        msg.attach(MIMEText(self.email_body, 'html'))
        return msg

    def _attach(self, msg=None):
        if msg is None:
            self._create_message()

        if self.attachment != "":
            for item in self.attachment:
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(item, "rb").read())
                Encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment', filename=item.split('/')[-1])
                msg.attach(part)
        return msg

    def _send(self, msg=None, server=None):
        if msg is None:
            msg = self._create_message()

        if server is None:
            server = self._create_server()

        server.sendmail(self.from_addr, self.to_addr, msg.as_string())
        return
