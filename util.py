def build_query(query_string, key_mappings):
    for k, v in key_mappings.iteritems():
        query_string = query_string.replace(k, v)
    return query_string