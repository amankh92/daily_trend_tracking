DB_URL = "jdbc:redshift://dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw"
MASTER_USERNAME = "datasciences_ro"
MASTER_PASSWORD = "DaTaScIences@123ro"
DB_NAME = "myntra_dw"
PORT = "5439"
HOST = "dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com"
WINDOW = 100
METRICS_MEANS_FILENAME = "metrics_means.csv"
ROI_FACTOR = 0.75
REV_FACTOR = 1
RGM_FACTOR = 1
DISC_FACTOR = 1
BASELINE_WEIGHT = 0.3
TRACKER_FILE_PATH = '/Users/11138/PycharmProjects/daily_trend_tracking/trend_tracker.csv'
CATEGORIES_FILE_PATH = '/Users/11138/PycharmProjects/daily_trend_tracking/categories1.csv'
METRICS_QUERY = '''
SELECT
    metrics.*,
    (metrics.rev_c_perc * metrics.rgm_c_perc / metrics.disc_c_perc) as roi,
    dd.day_string,
    dd.day_of_the_week
FROM
    dim_date dd
JOIN
(
    SELECT
        moda.date,
        (moda.moda_rev/myntra.total_rev) * 100::DECIMAL(10, 2) as rev_c_perc,
        (moda.moda_rgm/myntra.total_rgm) * 100::DECIMAL(10, 2) as rgm_c_perc,
        (moda.moda_disc/myntra.total_disc) * 100::DECIMAL(10, 2) as disc_c_perc
    FROM
        (
            SELECT
                fci.order_created_date as date,
                sum(fci.item_revenue_inc_cashback) as total_rev,
                sum(fci.rgm_final) as total_rgm,
                SUM(fci.product_discount) as total_disc
            FROM
                fact_core_item fci
            WHERE
                fci.order_created_date BETWEEN [START_DATE] and [END_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
            group by
                date
            order by
                date
        ) myntra
    JOIN
        (
            SELECT
                fci.order_created_date as date,
                SUM(fci.item_revenue_inc_cashback) as moda_rev,
                SUM(fci.rgm_final) as moda_rgm,
                SUM(fci.product_discount) as moda_disc
            from
                fact_core_item fci
            WHERE
                fci.order_created_date BETWEEN [START_DATE] and [END_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
                and fci.brand in ('Moda Rapido', 'Moda Rapido Disney', 'Moda Rapido Marvel', 'Moda Rapido Star Wars')
                and fci.article_type = 'Tshirts'
            group by
                date
            order by
                date
        ) moda
    on moda.date=myntra.date
    order by
    moda.date
) metrics
on metrics.date=dd.full_date
where dd.day_of_the_week < 6
order by metrics.date
'''

METRICS_QUERY_4HOURLY = '''
SELECT
    metrics.*,
    (metrics.rev_c_perc_04am * metrics.rgm_c_perc_04am / metrics.disc_c_perc_04am) as roi_04am,
    (metrics.rev_c_perc_08am * metrics.rgm_c_perc_08am / metrics.disc_c_perc_08am) as roi_08am,
    (metrics.rev_c_perc_12pm * metrics.rgm_c_perc_12pm / metrics.disc_c_perc_12pm) as roi_12pm,
    (metrics.rev_c_perc_04pm * metrics.rgm_c_perc_04pm / metrics.disc_c_perc_04pm) as roi_04pm,
    (metrics.rev_c_perc_08pm * metrics.rgm_c_perc_08pm / metrics.disc_c_perc_08pm) as roi_08pm,
    (metrics.rev_c_perc_11pm * metrics.rgm_c_perc_11pm / metrics.disc_c_perc_11pm) as roi_11pm,
    dd.day_string,
    dd.day_of_the_week
FROM
    dim_date dd
JOIN
(
    SELECT
        moda.date,
        (moda.moda_rev_04am/myntra.total_rev_04am) * 100::DECIMAL(10, 2) as rev_c_perc_04am,
        (moda.moda_rgm_04am/myntra.total_rgm_04am) * 100::DECIMAL(10, 2) as rgm_c_perc_04am,
        (moda.moda_disc_04am/myntra.total_disc_04am) * 100::DECIMAL(10, 2) as disc_c_perc_04am,
        (moda.moda_rev_08am/myntra.total_rev_08am) * 100::DECIMAL(10, 2) as rev_c_perc_08am,
        (moda.moda_rgm_08am/myntra.total_rgm_08am) * 100::DECIMAL(10, 2) as rgm_c_perc_08am,
        (moda.moda_disc_08am/myntra.total_disc_08am) * 100::DECIMAL(10, 2) as disc_c_perc_08am,
        (moda.moda_rev_12pm/myntra.total_rev_12pm) * 100::DECIMAL(10, 2) as rev_c_perc_12pm,
        (moda.moda_rgm_12pm/myntra.total_rgm_12pm) * 100::DECIMAL(10, 2) as rgm_c_perc_12pm,
        (moda.moda_disc_12pm/myntra.total_disc_12pm) * 100::DECIMAL(10, 2) as disc_c_perc_12pm,
        (moda.moda_rev_04pm/myntra.total_rev_04pm) * 100::DECIMAL(10, 2) as rev_c_perc_04pm,
        (moda.moda_rgm_04pm/myntra.total_rgm_04pm) * 100::DECIMAL(10, 2) as rgm_c_perc_04pm,
        (moda.moda_disc_04pm/myntra.total_disc_04pm) * 100::DECIMAL(10, 2) as disc_c_perc_04pm,
        (moda.moda_rev_08pm/myntra.total_rev_08pm) * 100::DECIMAL(10, 2) as rev_c_perc_08pm,
        (moda.moda_rgm_08pm/myntra.total_rgm_08pm) * 100::DECIMAL(10, 2) as rgm_c_perc_08pm,
        (moda.moda_disc_08pm/myntra.total_disc_08pm) * 100::DECIMAL(10, 2) as disc_c_perc_08pm,
        (moda.moda_rev_11pm/myntra.total_rev_11pm) * 100::DECIMAL(10, 2) as rev_c_perc_11pm,
        (moda.moda_rgm_11pm/myntra.total_rgm_11pm) * 100::DECIMAL(10, 2) as rgm_c_perc_11pm,
        (moda.moda_disc_11pm/myntra.total_disc_11pm) * 100::DECIMAL(10, 2) as disc_c_perc_11pm
    FROM
        (
            SELECT
                fci.order_created_date as date,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_12pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_04pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.item_revenue_inc_cashback else 0 END) as total_rev_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.rgm_final else 0 END) as total_rgm_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.rgm_final else 0 END) as total_rgm_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.rgm_final else 0 END) as total_rgm_12pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.rgm_final else 0 END) as total_rgm_04pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.rgm_final else 0 END) as total_rgm_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.rgm_final else 0 END) as total_rgm_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.product_discount else 0 END) as total_disc_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.product_discount else 0 END) as total_disc_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.product_discount else 0 END) as total_disc_12pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.product_discount else 0 END) as total_disc_04pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.product_discount else 0 END) as total_disc_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.product_discount else 0 END) as total_disc_11pm
            FROM
                fact_core_item fci
            WHERE
                fci.order_created_date BETWEEN [START_DATE] and [END_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
            group by
                date
            order by
                date
        ) myntra
    JOIN
        (
            SELECT
                fci.order_created_date as date,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_12pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_04pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.rgm_final else 0 END) as moda_rgm_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.rgm_final else 0 END) as moda_rgm_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.rgm_final else 0 END) as moda_rgm_12pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.rgm_final else 0 END) as moda_rgm_04pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.rgm_final else 0 END) as moda_rgm_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.rgm_final else 0 END) as moda_rgm_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.product_discount else 0 END) as moda_disc_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.product_discount else 0 END) as moda_disc_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.product_discount else 0 END) as moda_disc_12pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.product_discount else 0 END) as moda_disc_04pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.product_discount else 0 END) as moda_disc_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.product_discount else 0 END) as moda_disc_11pm
            from
                fact_core_item fci
            WHERE
                fci.order_created_date BETWEEN [START_DATE] and [END_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
                and fci.brand in ('Moda Rapido', 'Moda Rapido Disney', 'Moda Rapido Marvel', 'Moda Rapido Star Wars')
                and fci.article_type = 'Tshirts'
            group by
                date
            order by
                date
        ) moda
    on moda.date=myntra.date
    order by
    moda.date
) metrics
on metrics.date=dd.full_date
where dd.day_of_the_week < 6
order by metrics.date
'''

METRICS_QUERY_NEW = '''
SELECT
    metrics.*,
    (metrics.rev_c_perc_04am * metrics.rgm_c_perc_04am / metrics.disc_c_perc_04am) as roi_04am,
    (metrics.rev_c_perc_08am * metrics.rgm_c_perc_08am / metrics.disc_c_perc_08am) as roi_08am,
    (metrics.rev_c_perc_12pm * metrics.rgm_c_perc_12pm / metrics.disc_c_perc_12pm) as roi_12pm,
    (metrics.rev_c_perc_02pm * metrics.rgm_c_perc_02pm / metrics.disc_c_perc_02pm) as roi_02pm,
    (metrics.rev_c_perc_04pm * metrics.rgm_c_perc_04pm / metrics.disc_c_perc_04pm) as roi_04pm,
    (metrics.rev_c_perc_06pm * metrics.rgm_c_perc_06pm / metrics.disc_c_perc_06pm) as roi_06pm,
    (metrics.rev_c_perc_08pm * metrics.rgm_c_perc_08pm / metrics.disc_c_perc_08pm) as roi_08pm,
    (metrics.rev_c_perc_11pm * metrics.rgm_c_perc_11pm / metrics.disc_c_perc_11pm) as roi_11pm,
    dd.day_string,
    dd.day_of_the_week
FROM
    dim_date dd
JOIN
(
    SELECT
        moda.date,
        (moda.moda_rev_04am/myntra.total_rev_04am) * 100::DECIMAL(10, 2) as rev_c_perc_04am,
        (moda.moda_rgm_04am/myntra.total_rgm_04am) * 100::DECIMAL(10, 2) as rgm_c_perc_04am,
        (moda.moda_disc_04am/myntra.total_disc_04am) * 100::DECIMAL(10, 2) as disc_c_perc_04am,
        (moda.moda_rev_08am/myntra.total_rev_08am) * 100::DECIMAL(10, 2) as rev_c_perc_08am,
        (moda.moda_rgm_08am/myntra.total_rgm_08am) * 100::DECIMAL(10, 2) as rgm_c_perc_08am,
        (moda.moda_disc_08am/myntra.total_disc_08am) * 100::DECIMAL(10, 2) as disc_c_perc_08am,
        (moda.moda_rev_12pm/myntra.total_rev_12pm) * 100::DECIMAL(10, 2) as rev_c_perc_12pm,
        (moda.moda_rgm_12pm/myntra.total_rgm_12pm) * 100::DECIMAL(10, 2) as rgm_c_perc_12pm,
        (moda.moda_disc_12pm/myntra.total_disc_12pm) * 100::DECIMAL(10, 2) as disc_c_perc_12pm,
        (moda.moda_rev_02pm/myntra.total_rev_02pm) * 100::DECIMAL(10, 2) as rev_c_perc_02pm,
        (moda.moda_rgm_02pm/myntra.total_rgm_02pm) * 100::DECIMAL(10, 2) as rgm_c_perc_02pm,
        (moda.moda_disc_02pm/myntra.total_disc_02pm) * 100::DECIMAL(10, 2) as disc_c_perc_02pm,
        (moda.moda_rev_04pm/myntra.total_rev_04pm) * 100::DECIMAL(10, 2) as rev_c_perc_04pm,
        (moda.moda_rgm_04pm/myntra.total_rgm_04pm) * 100::DECIMAL(10, 2) as rgm_c_perc_04pm,
        (moda.moda_disc_04pm/myntra.total_disc_04pm) * 100::DECIMAL(10, 2) as disc_c_perc_04pm,
        (moda.moda_rev_06pm/myntra.total_rev_06pm) * 100::DECIMAL(10, 2) as rev_c_perc_06pm,
        (moda.moda_rgm_06pm/myntra.total_rgm_06pm) * 100::DECIMAL(10, 2) as rgm_c_perc_06pm,
        (moda.moda_disc_06pm/myntra.total_disc_06pm) * 100::DECIMAL(10, 2) as disc_c_perc_06pm,
        (moda.moda_rev_08pm/myntra.total_rev_08pm) * 100::DECIMAL(10, 2) as rev_c_perc_08pm,
        (moda.moda_rgm_08pm/myntra.total_rgm_08pm) * 100::DECIMAL(10, 2) as rgm_c_perc_08pm,
        (moda.moda_disc_08pm/myntra.total_disc_08pm) * 100::DECIMAL(10, 2) as disc_c_perc_08pm,
        (moda.moda_rev_11pm/myntra.total_rev_11pm) * 100::DECIMAL(10, 2) as rev_c_perc_11pm,
        (moda.moda_rgm_11pm/myntra.total_rgm_11pm) * 100::DECIMAL(10, 2) as rgm_c_perc_11pm,
        (moda.moda_disc_11pm/myntra.total_disc_11pm) * 100::DECIMAL(10, 2) as disc_c_perc_11pm
    FROM
        (
            SELECT
                fci.order_created_date as date,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.item_revenue_inc_cashback else 0 END) as total_rev_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.rgm_final else 0 END) as total_rgm_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.rgm_final else 0 END) as total_rgm_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.rgm_final else 0 END) as total_rgm_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.rgm_final else 0 END) as total_rgm_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.rgm_final else 0 END) as total_rgm_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.rgm_final else 0 END) as total_rgm_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.rgm_final else 0 END) as total_rgm_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.rgm_final else 0 END) as total_rgm_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.product_discount else 0 END) as total_disc_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.product_discount else 0 END) as total_disc_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.product_discount else 0 END) as total_disc_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.product_discount else 0 END) as total_disc_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.product_discount else 0 END) as total_disc_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.product_discount else 0 END) as total_disc_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.product_discount else 0 END) as total_disc_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.product_discount else 0 END) as total_disc_11pm
            FROM
                fact_core_item fci
            WHERE
                fci.order_created_date BETWEEN [START_DATE] and [END_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
            group by
                date
            order by
                date
        ) myntra
    JOIN
        (
            SELECT
                fci.order_created_date as date,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.rgm_final else 0 END) as moda_rgm_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.rgm_final else 0 END) as moda_rgm_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.rgm_final else 0 END) as moda_rgm_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.rgm_final else 0 END) as moda_rgm_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.rgm_final else 0 END) as moda_rgm_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.rgm_final else 0 END) as moda_rgm_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.rgm_final else 0 END) as moda_rgm_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.rgm_final else 0 END) as moda_rgm_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.product_discount else 0 END) as moda_disc_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.product_discount else 0 END) as moda_disc_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.product_discount else 0 END) as moda_disc_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.product_discount else 0 END) as moda_disc_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.product_discount else 0 END) as moda_disc_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.product_discount else 0 END) as moda_disc_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.product_discount else 0 END) as moda_disc_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.product_discount else 0 END) as moda_disc_11pm
            from
                fact_core_item fci
            WHERE
                fci.order_created_date BETWEEN [START_DATE] and [END_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
                and fci.brand in ('Moda Rapido', 'Moda Rapido Disney', 'Moda Rapido Marvel', 'Moda Rapido Star Wars')
                and fci.article_type = 'Tshirts'
            group by
                date
            order by
                date
        ) moda
    on moda.date=myntra.date
    order by
    moda.date
) metrics
on metrics.date=dd.full_date
where dd.day_of_the_week < 6
order by metrics.date
'''

METRICS_MOST_RECENT_BASELINE = '''
SELECT
    metrics.*,
    (metrics.rev_c_perc_04am * metrics.rgm_c_perc_04am / metrics.disc_c_perc_04am) as roi_04am,
    (metrics.rev_c_perc_08am * metrics.rgm_c_perc_08am / metrics.disc_c_perc_08am) as roi_08am,
    (metrics.rev_c_perc_12pm * metrics.rgm_c_perc_12pm / metrics.disc_c_perc_12pm) as roi_12pm,
    (metrics.rev_c_perc_02pm * metrics.rgm_c_perc_02pm / metrics.disc_c_perc_02pm) as roi_02pm,
    (metrics.rev_c_perc_04pm * metrics.rgm_c_perc_04pm / metrics.disc_c_perc_04pm) as roi_04pm,
    (metrics.rev_c_perc_06pm * metrics.rgm_c_perc_06pm / metrics.disc_c_perc_06pm) as roi_06pm,
    (metrics.rev_c_perc_08pm * metrics.rgm_c_perc_08pm / metrics.disc_c_perc_08pm) as roi_08pm,
    (metrics.rev_c_perc_11pm * metrics.rgm_c_perc_11pm / metrics.disc_c_perc_11pm) as roi_11pm,
    dd.day_string,
    dd.day_of_the_week
FROM
    dim_date dd
JOIN
(
    SELECT
        moda.date,
        (moda.moda_rev_04am/myntra.total_rev_04am) * 100::DECIMAL(10, 2) as rev_c_perc_04am,
        (moda.moda_rgm_04am/myntra.total_rgm_04am) * 100::DECIMAL(10, 2) as rgm_c_perc_04am,
        (moda.moda_disc_04am/myntra.total_disc_04am) * 100::DECIMAL(10, 2) as disc_c_perc_04am,
        (moda.moda_rev_08am/myntra.total_rev_08am) * 100::DECIMAL(10, 2) as rev_c_perc_08am,
        (moda.moda_rgm_08am/myntra.total_rgm_08am) * 100::DECIMAL(10, 2) as rgm_c_perc_08am,
        (moda.moda_disc_08am/myntra.total_disc_08am) * 100::DECIMAL(10, 2) as disc_c_perc_08am,
        (moda.moda_rev_12pm/myntra.total_rev_12pm) * 100::DECIMAL(10, 2) as rev_c_perc_12pm,
        (moda.moda_rgm_12pm/myntra.total_rgm_12pm) * 100::DECIMAL(10, 2) as rgm_c_perc_12pm,
        (moda.moda_disc_12pm/myntra.total_disc_12pm) * 100::DECIMAL(10, 2) as disc_c_perc_12pm,
        (moda.moda_rev_02pm/myntra.total_rev_02pm) * 100::DECIMAL(10, 2) as rev_c_perc_02pm,
        (moda.moda_rgm_02pm/myntra.total_rgm_02pm) * 100::DECIMAL(10, 2) as rgm_c_perc_02pm,
        (moda.moda_disc_02pm/myntra.total_disc_02pm) * 100::DECIMAL(10, 2) as disc_c_perc_02pm,
        (moda.moda_rev_04pm/myntra.total_rev_04pm) * 100::DECIMAL(10, 2) as rev_c_perc_04pm,
        (moda.moda_rgm_04pm/myntra.total_rgm_04pm) * 100::DECIMAL(10, 2) as rgm_c_perc_04pm,
        (moda.moda_disc_04pm/myntra.total_disc_04pm) * 100::DECIMAL(10, 2) as disc_c_perc_04pm,
        (moda.moda_rev_06pm/myntra.total_rev_06pm) * 100::DECIMAL(10, 2) as rev_c_perc_06pm,
        (moda.moda_rgm_06pm/myntra.total_rgm_06pm) * 100::DECIMAL(10, 2) as rgm_c_perc_06pm,
        (moda.moda_disc_06pm/myntra.total_disc_06pm) * 100::DECIMAL(10, 2) as disc_c_perc_06pm,
        (moda.moda_rev_08pm/myntra.total_rev_08pm) * 100::DECIMAL(10, 2) as rev_c_perc_08pm,
        (moda.moda_rgm_08pm/myntra.total_rgm_08pm) * 100::DECIMAL(10, 2) as rgm_c_perc_08pm,
        (moda.moda_disc_08pm/myntra.total_disc_08pm) * 100::DECIMAL(10, 2) as disc_c_perc_08pm,
        (moda.moda_rev_11pm/myntra.total_rev_11pm) * 100::DECIMAL(10, 2) as rev_c_perc_11pm,
        (moda.moda_rgm_11pm/myntra.total_rgm_11pm) * 100::DECIMAL(10, 2) as rgm_c_perc_11pm,
        (moda.moda_disc_11pm/myntra.total_disc_11pm) * 100::DECIMAL(10, 2) as disc_c_perc_11pm
    FROM
        (
            SELECT
                fci.order_created_date as date,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.item_revenue_inc_cashback else 0 END) as total_rev_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.rgm_final else 0 END) as total_rgm_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.rgm_final else 0 END) as total_rgm_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.rgm_final else 0 END) as total_rgm_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.rgm_final else 0 END) as total_rgm_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.rgm_final else 0 END) as total_rgm_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.rgm_final else 0 END) as total_rgm_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.rgm_final else 0 END) as total_rgm_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.rgm_final else 0 END) as total_rgm_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.product_discount else 0 END) as total_disc_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.product_discount else 0 END) as total_disc_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.product_discount else 0 END) as total_disc_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.product_discount else 0 END) as total_disc_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.product_discount else 0 END) as total_disc_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.product_discount else 0 END) as total_disc_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.product_discount else 0 END) as total_disc_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.product_discount else 0 END) as total_disc_11pm
            FROM
                fact_core_item fci
            WHERE
                fci.order_created_date=[START_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
            group by
                date
            order by
                date
        ) myntra
    JOIN
        (
            SELECT
                fci.order_created_date as date,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.rgm_final else 0 END) as moda_rgm_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.rgm_final else 0 END) as moda_rgm_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.rgm_final else 0 END) as moda_rgm_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.rgm_final else 0 END) as moda_rgm_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.rgm_final else 0 END) as moda_rgm_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.rgm_final else 0 END) as moda_rgm_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.rgm_final else 0 END) as moda_rgm_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.rgm_final else 0 END) as moda_rgm_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.product_discount else 0 END) as moda_disc_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.product_discount else 0 END) as moda_disc_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.product_discount else 0 END) as moda_disc_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.product_discount else 0 END) as moda_disc_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.product_discount else 0 END) as moda_disc_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.product_discount else 0 END) as moda_disc_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.product_discount else 0 END) as moda_disc_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.product_discount else 0 END) as moda_disc_11pm
            from
                fact_core_item fci
            WHERE
                fci.order_created_date=[START_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
                and fci.brand in ('Moda Rapido', 'Moda Rapido Disney', 'Moda Rapido Marvel', 'Moda Rapido Star Wars')
                and fci.article_type = 'Tshirts'
            group by
                date
            order by
                date
        ) moda
    on moda.date=myntra.date
    order by
    moda.date
) metrics
on metrics.date=dd.full_date
order by metrics.date
'''

INTRA_DAY_TREND_TRACKING_QUERY = '''
SELECT
    metrics.*,
    (metrics.rev_c_perc * metrics.rgm_c_perc / metrics.disc_c_perc) as roi,
    dd.day_string,
    dd.day_of_the_week
FROM
    dim_date dd
JOIN
(
    SELECT
        moda.date,
        (moda.moda_rev/myntra.total_rev) * 100 as rev_c_perc,
        (moda.moda_rgm/myntra.total_rgm) * 100 as rgm_c_perc,
        (moda.moda_disc/myntra.total_disc) * 100 as disc_c_perc
    FROM
        (
            SELECT
                fci.order_created_date as date,
                sum(item_revenue - tax_amount) as total_rev,
                sum(item_revenue - tax_amount - (item_purchase_price_inc_tax - vendor_funding + royalty_commission)) as total_rgm,
                SUM(fci.trade_discount) as total_disc
            FROM
                fact_order_live fci
            WHERE
                fci.order_created_date=[START_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
                and fci.order_created_time<=[HOUR]
            group by
                date
            order by
                date
        ) myntra
    JOIN
        (
            SELECT
                fci.order_created_date as date,
                sum(item_revenue - tax_amount) as moda_rev,
                sum(item_revenue - tax_amount - (item_purchase_price_inc_tax - vendor_funding + royalty_commission)) as moda_rgm,
                SUM(fci.trade_discount) as moda_disc
            from
                fact_order_live fci
            WHERE
                fci.order_created_date=[START_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
                and fci.brand in ('Moda Rapido', 'Moda Rapido Disney', 'Moda Rapido Marvel', 'Moda Rapido Star Wars')
                and fci.article_type ='[ARTICLE_TYPE]'
                and fci.gender='[GENDER]'
                and fci.order_created_time<=[HOUR]
            group by
                date
            order by
                date
        ) moda
    on moda.date=myntra.date
    order by
    moda.date
) metrics
on metrics.date=dd.full_date
where dd.day_of_the_week < 6
order by metrics.date
'''


TIME_SUFFIXES = {
    4: '_04am',
    8: '_08am',
    12: '_12pm',
    14: '_02pm',
    16: '_04pm',
    18: '_06pm',
    20: '_08pm',
    23: '_11pm'
}
