import psycopg2.extras
from config import *
import datetime
import util
import pandas
import numpy as np
import csv
import queries


def generate_filename(brand, article_type, gender):
    return brand.replace(" ", "").lower() + "_" + article_type.replace(" ", "").lower() + "_" + gender.lower() + "_" + METRICS_MEANS_FILENAME


def get_connection():
    try:
        conn = psycopg2.connect("dbname='%s' port='%s' user='%s' host='%s' password='%s'" % (
            DB_NAME, PORT, MASTER_USERNAME, HOST, MASTER_PASSWORD))
    except Exception as e:
        print e
    return conn


def get_brand_name(brand):
    if brand == "Moda Rapido":
        return "('Moda Rapido', 'Moda Rapido Disney', 'Moda Rapido Marvel', 'Moda Rapido Star Wars')"
    else:
        return "('" + brand + "')"


def get_metrics(brand, article_type, gender):
    conn = get_connection()
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    end_date = (datetime.datetime.today() - datetime.timedelta(days=1))
    start_date = end_date - datetime.timedelta(days=WINDOW)

    key_mappings = {
        '[START_DATE]': start_date.strftime('%Y%m%d'),
        '[END_DATE]': end_date.strftime('%Y%m%d'),
        '[ARTICLE_TYPE]': article_type,
        '[GENDER]': gender,
        '[BRAND]': get_brand_name(brand)
    }

    query_string = util.build_query(queries.METRICS_QUERY_NEW, key_mappings)

    try:
        cur.execute(query_string)
    except:
        print "unable to execute query"

    rows = cur.fetchall()

    metrics_df = pandas.DataFrame([i.copy() for i in rows])

    metrics_df.iloc[:, 3:] = metrics_df.iloc[:, 3:].astype(float)
    metrics_df.iloc[:, 3:] = np.round(metrics_df.iloc[:, 3:], 2)

    mean_df = metrics_df.iloc[:, 3:].mean()
    sd_df = metrics_df.iloc[:, 3:].std(axis=0)

    mean_df = mean_df.reset_index()
    sd_df = sd_df.reset_index()

    mean_df = mean_df.rename(columns={0: 'mean'})
    sd_df = sd_df.rename(columns={0: 'sd'})
    final_df = pandas.merge(mean_df, sd_df, how='inner', on='index')
    final_df.to_csv(generate_filename(brand, article_type, gender), index=False)


if __name__ == "__main__":
    with open(CATEGORIES_FILE_PATH, 'r') as f:
        csv_dict = csv.DictReader(f)
        for obj in csv_dict:
            print "Querying for " + obj['brand'] + " " + obj['article_type'] + " " + obj['gender']
            get_metrics(obj['brand'], obj['article_type'], obj['gender'])
