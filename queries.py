METRICS_MOST_RECENT_BASELINE = '''
SELECT
    metrics.*,
    round((metrics.rev_c_perc_04am * metrics.rgm_c_perc_04am / metrics.disc_c_perc_04am),4) as roi_04am,
    round((metrics.rev_c_perc_08am * metrics.rgm_c_perc_08am / metrics.disc_c_perc_08am),4) as roi_08am,
    round((metrics.rev_c_perc_12pm * metrics.rgm_c_perc_12pm / metrics.disc_c_perc_12pm),4) as roi_12pm,
    round((metrics.rev_c_perc_02pm * metrics.rgm_c_perc_02pm / metrics.disc_c_perc_02pm),4) as roi_02pm,
    round((metrics.rev_c_perc_04pm * metrics.rgm_c_perc_04pm / metrics.disc_c_perc_04pm),4) as roi_04pm,
    round((metrics.rev_c_perc_06pm * metrics.rgm_c_perc_06pm / metrics.disc_c_perc_06pm),4) as roi_06pm,
    round((metrics.rev_c_perc_08pm * metrics.rgm_c_perc_08pm / metrics.disc_c_perc_08pm),4) as roi_08pm,
    round((metrics.rev_c_perc_11pm * metrics.rgm_c_perc_11pm / metrics.disc_c_perc_11pm),4) as roi_11pm,
    dd.day_string,
    dd.day_of_the_week
FROM
    dim_date dd
JOIN
(
    SELECT
        moda.date,
        round((moda.moda_rev_04am/myntra.total_rev_04am),4) * 100 as rev_c_perc_04am,
        round((moda.moda_rgm_04am/myntra.total_rgm_04am),4) * 100 as rgm_c_perc_04am,
        round((moda.moda_disc_04am/myntra.total_disc_04am),4) * 100 as disc_c_perc_04am,
        round((moda.moda_rev_08am/myntra.total_rev_08am),4) * 100 as rev_c_perc_08am,
        round((moda.moda_rgm_08am/myntra.total_rgm_08am),4) * 100 as rgm_c_perc_08am,
        round((moda.moda_disc_08am/myntra.total_disc_08am),4) * 100 as disc_c_perc_08am,
        round((moda.moda_rev_12pm/myntra.total_rev_12pm),4) * 100 as rev_c_perc_12pm,
        round((moda.moda_rgm_12pm/myntra.total_rgm_12pm),4) * 100 as rgm_c_perc_12pm,
        round((moda.moda_disc_12pm/myntra.total_disc_12pm),4) * 100 as disc_c_perc_12pm,
        round((moda.moda_rev_02pm/myntra.total_rev_02pm),4) * 100 as rev_c_perc_02pm,
        round((moda.moda_rgm_02pm/myntra.total_rgm_02pm),4) * 100 as rgm_c_perc_02pm,
        round((moda.moda_disc_02pm/myntra.total_disc_02pm),4) * 100 as disc_c_perc_02pm,
        round((moda.moda_rev_04pm/myntra.total_rev_04pm),4) * 100 as rev_c_perc_04pm,
        round((moda.moda_rgm_04pm/myntra.total_rgm_04pm),4) * 100 as rgm_c_perc_04pm,
        round((moda.moda_disc_04pm/myntra.total_disc_04pm),4) * 100 as disc_c_perc_04pm,
        round((moda.moda_rev_06pm/myntra.total_rev_06pm),4) * 100 as rev_c_perc_06pm,
        round((moda.moda_rgm_06pm/myntra.total_rgm_06pm),4) * 100 as rgm_c_perc_06pm,
        round((moda.moda_disc_06pm/myntra.total_disc_06pm),4) * 100 as disc_c_perc_06pm,
        round((moda.moda_rev_08pm/myntra.total_rev_08pm),4) * 100 as rev_c_perc_08pm,
        round((moda.moda_rgm_08pm/myntra.total_rgm_08pm),4) * 100 as rgm_c_perc_08pm,
        round((moda.moda_disc_08pm/myntra.total_disc_08pm),4) * 100 as disc_c_perc_08pm,
        round((moda.moda_rev_11pm/myntra.total_rev_11pm),4) * 100 as rev_c_perc_11pm,
        round((moda.moda_rgm_11pm/myntra.total_rgm_11pm),4) * 100 as rgm_c_perc_11pm,
        round((moda.moda_disc_11pm/myntra.total_disc_11pm),4) * 100 as disc_c_perc_11pm
    FROM
        (
            SELECT
                fci.order_created_date as date,
                round(sum(CASE WHEN fci.order_created_time <= 040000 then fci.item_revenue_inc_cashback else 0 END),4) as total_rev_04am,
                round(sum(CASE WHEN fci.order_created_time <= 080000 then fci.item_revenue_inc_cashback else 0 END),4) as total_rev_08am,
                round(sum(CASE WHEN fci.order_created_time <= 120000 then fci.item_revenue_inc_cashback else 0 END),4) as total_rev_12pm,
                round(sum(CASE WHEN fci.order_created_time <= 140000 then fci.item_revenue_inc_cashback else 0 END),4) as total_rev_02pm,
                round(sum(CASE WHEN fci.order_created_time <= 160000 then fci.item_revenue_inc_cashback else 0 END),4) as total_rev_04pm,
                round(sum(CASE WHEN fci.order_created_time <= 180000 then fci.item_revenue_inc_cashback else 0 END),4) as total_rev_06pm,
                round(sum(CASE WHEN fci.order_created_time <= 200000 then fci.item_revenue_inc_cashback else 0 END),4) as total_rev_08pm,
                round(sum(CASE WHEN fci.order_created_time <= 235959 then fci.item_revenue_inc_cashback else 0 END),4) as total_rev_11pm,
                round(sum(CASE WHEN fci.order_created_time <= 040000 then fci.rgm_final else 0 END),4) as total_rgm_04am,
                round(sum(CASE WHEN fci.order_created_time <= 080000 then fci.rgm_final else 0 END),4) as total_rgm_08am,
                round(sum(CASE WHEN fci.order_created_time <= 120000 then fci.rgm_final else 0 END),4) as total_rgm_12pm,
                round(sum(CASE WHEN fci.order_created_time <= 140000 then fci.rgm_final else 0 END),4) as total_rgm_02pm,
                round(sum(CASE WHEN fci.order_created_time <= 160000 then fci.rgm_final else 0 END),4) as total_rgm_04pm,
                round(sum(CASE WHEN fci.order_created_time <= 180000 then fci.rgm_final else 0 END),4) as total_rgm_06pm,
                round(sum(CASE WHEN fci.order_created_time <= 200000 then fci.rgm_final else 0 END),4) as total_rgm_08pm,
                round(sum(CASE WHEN fci.order_created_time <= 235959 then fci.rgm_final else 0 END),4) as total_rgm_11pm,
                round(sum(CASE WHEN fci.order_created_time <= 040000 then fci.product_discount else 0 END),4) as total_disc_04am,
                round(sum(CASE WHEN fci.order_created_time <= 080000 then fci.product_discount else 0 END),4) as total_disc_08am,
                round(sum(CASE WHEN fci.order_created_time <= 120000 then fci.product_discount else 0 END),4) as total_disc_12pm,
                round(sum(CASE WHEN fci.order_created_time <= 140000 then fci.product_discount else 0 END),4) as total_disc_02pm,
                round(sum(CASE WHEN fci.order_created_time <= 160000 then fci.product_discount else 0 END),4) as total_disc_04pm,
                round(sum(CASE WHEN fci.order_created_time <= 180000 then fci.product_discount else 0 END),4) as total_disc_06pm,
                round(sum(CASE WHEN fci.order_created_time <= 200000 then fci.product_discount else 0 END),4) as total_disc_08pm,
                round(sum(CASE WHEN fci.order_created_time <= 235959 then fci.product_discount else 0 END),4) as total_disc_11pm
            FROM
                fact_core_item fci
            WHERE
                fci.order_created_date=[START_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
            group by
                date
            order by
                date
        ) myntra
    JOIN
        (
            SELECT
                fci.order_created_date as date,
                round(sum(CASE WHEN fci.order_created_time <= 040000 then fci.item_revenue_inc_cashback else 0 END),4) as moda_rev_04am,
                round(sum(CASE WHEN fci.order_created_time <= 080000 then fci.item_revenue_inc_cashback else 0 END),4) as moda_rev_08am,
                round(sum(CASE WHEN fci.order_created_time <= 120000 then fci.item_revenue_inc_cashback else 0 END),4) as moda_rev_12pm,
                round(sum(CASE WHEN fci.order_created_time <= 140000 then fci.item_revenue_inc_cashback else 0 END),4) as moda_rev_02pm,
                round(sum(CASE WHEN fci.order_created_time <= 160000 then fci.item_revenue_inc_cashback else 0 END),4) as moda_rev_04pm,
                round(sum(CASE WHEN fci.order_created_time <= 180000 then fci.item_revenue_inc_cashback else 0 END),4) as moda_rev_06pm,
                round(sum(CASE WHEN fci.order_created_time <= 200000 then fci.item_revenue_inc_cashback else 0 END),4) as moda_rev_08pm,
                round(sum(CASE WHEN fci.order_created_time <= 235959 then fci.item_revenue_inc_cashback else 0 END),4) as moda_rev_11pm,
                round(sum(CASE WHEN fci.order_created_time <= 040000 then fci.rgm_final else 0 END),4) as moda_rgm_04am,
                round(sum(CASE WHEN fci.order_created_time <= 080000 then fci.rgm_final else 0 END),4) as moda_rgm_08am,
                round(sum(CASE WHEN fci.order_created_time <= 120000 then fci.rgm_final else 0 END),4) as moda_rgm_12pm,
                round(sum(CASE WHEN fci.order_created_time <= 140000 then fci.rgm_final else 0 END),4) as moda_rgm_02pm,
                round(sum(CASE WHEN fci.order_created_time <= 160000 then fci.rgm_final else 0 END),4) as moda_rgm_04pm,
                round(sum(CASE WHEN fci.order_created_time <= 180000 then fci.rgm_final else 0 END),4) as moda_rgm_06pm,
                round(sum(CASE WHEN fci.order_created_time <= 200000 then fci.rgm_final else 0 END),4) as moda_rgm_08pm,
                round(sum(CASE WHEN fci.order_created_time <= 235959 then fci.rgm_final else 0 END),4) as moda_rgm_11pm,
                round(sum(CASE WHEN fci.order_created_time <= 040000 then fci.product_discount else 0 END),4) as moda_disc_04am,
                round(sum(CASE WHEN fci.order_created_time <= 080000 then fci.product_discount else 0 END),4) as moda_disc_08am,
                round(sum(CASE WHEN fci.order_created_time <= 120000 then fci.product_discount else 0 END),4) as moda_disc_12pm,
                round(sum(CASE WHEN fci.order_created_time <= 140000 then fci.product_discount else 0 END),4) as moda_disc_02pm,
                round(sum(CASE WHEN fci.order_created_time <= 160000 then fci.product_discount else 0 END),4) as moda_disc_04pm,
                round(sum(CASE WHEN fci.order_created_time <= 180000 then fci.product_discount else 0 END),4) as moda_disc_06pm,
                round(sum(CASE WHEN fci.order_created_time <= 200000 then fci.product_discount else 0 END),4) as moda_disc_08pm,
                round(sum(CASE WHEN fci.order_created_time <= 235959 then fci.product_discount else 0 END),4) as moda_disc_11pm
            from
                fact_core_item fci
            WHERE
                fci.order_created_date=[START_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
                and fci.brand in [BRAND]
                and fci.article_type = '[ARTICLE_TYPE]'
                and fci.gender = '[GENDER]'
            group by
                date
            order by
                date
        ) moda
    on moda.date=myntra.date
    order by
    moda.date
) metrics
on metrics.date=dd.full_date
order by metrics.date
'''

METRICS_QUERY_NEW = '''
SELECT
    metrics.*,
    NVL((metrics.rev_c_perc_04am * metrics.rgm_c_perc_04am / NULLIF(metrics.disc_c_perc_04am, 0)),0) as roi_04am,
    NVL((metrics.rev_c_perc_08am * metrics.rgm_c_perc_08am / NULLIF(metrics.disc_c_perc_08am, 0)),0) as roi_08am,
    NVL((metrics.rev_c_perc_12pm * metrics.rgm_c_perc_12pm / NULLIF(metrics.disc_c_perc_12pm, 0)),0) as roi_12pm,
    NVL((metrics.rev_c_perc_02pm * metrics.rgm_c_perc_02pm / NULLIF(metrics.disc_c_perc_02pm, 0)),0) as roi_02pm,
    NVL((metrics.rev_c_perc_04pm * metrics.rgm_c_perc_04pm / NULLIF(metrics.disc_c_perc_04pm, 0)),0) as roi_04pm,
    NVL((metrics.rev_c_perc_06pm * metrics.rgm_c_perc_06pm / NULLIF(metrics.disc_c_perc_06pm, 0)),0) as roi_06pm,
    NVL((metrics.rev_c_perc_08pm * metrics.rgm_c_perc_08pm / NULLIF(metrics.disc_c_perc_08pm, 0)),0) as roi_08pm,
    NVL((metrics.rev_c_perc_11pm * metrics.rgm_c_perc_11pm / NULLIF(metrics.disc_c_perc_11pm, 0)),0) as roi_11pm,
    dd.day_string,
    dd.day_of_the_week
FROM
    dim_date dd
JOIN
(
    SELECT
        moda.date,
        (moda.moda_rev_04am/myntra.total_rev_04am) * 100 as rev_c_perc_04am,
        (moda.moda_rgm_04am/myntra.total_rgm_04am) * 100 as rgm_c_perc_04am,
        (moda.moda_disc_04am/myntra.total_disc_04am) * 100 as disc_c_perc_04am,
        (moda.moda_rev_08am/myntra.total_rev_08am) * 100 as rev_c_perc_08am,
        (moda.moda_rgm_08am/myntra.total_rgm_08am) * 100 as rgm_c_perc_08am,
        (moda.moda_disc_08am/myntra.total_disc_08am) * 100 as disc_c_perc_08am,
        (moda.moda_rev_12pm/myntra.total_rev_12pm) * 100 as rev_c_perc_12pm,
        (moda.moda_rgm_12pm/myntra.total_rgm_12pm) * 100 as rgm_c_perc_12pm,
        (moda.moda_disc_12pm/myntra.total_disc_12pm) * 100 as disc_c_perc_12pm,
        (moda.moda_rev_02pm/myntra.total_rev_02pm) * 100 as rev_c_perc_02pm,
        (moda.moda_rgm_02pm/myntra.total_rgm_02pm) * 100 as rgm_c_perc_02pm,
        (moda.moda_disc_02pm/myntra.total_disc_02pm) * 100 as disc_c_perc_02pm,
        (moda.moda_rev_04pm/myntra.total_rev_04pm) * 100 as rev_c_perc_04pm,
        (moda.moda_rgm_04pm/myntra.total_rgm_04pm) * 100 as rgm_c_perc_04pm,
        (moda.moda_disc_04pm/myntra.total_disc_04pm) * 100 as disc_c_perc_04pm,
        (moda.moda_rev_06pm/myntra.total_rev_06pm) * 100 as rev_c_perc_06pm,
        (moda.moda_rgm_06pm/myntra.total_rgm_06pm) * 100 as rgm_c_perc_06pm,
        (moda.moda_disc_06pm/myntra.total_disc_06pm) * 100 as disc_c_perc_06pm,
        (moda.moda_rev_08pm/myntra.total_rev_08pm) * 100 as rev_c_perc_08pm,
        (moda.moda_rgm_08pm/myntra.total_rgm_08pm) * 100 as rgm_c_perc_08pm,
        (moda.moda_disc_08pm/myntra.total_disc_08pm) * 100 as disc_c_perc_08pm,
        (moda.moda_rev_11pm/myntra.total_rev_11pm) * 100 as rev_c_perc_11pm,
        (moda.moda_rgm_11pm/myntra.total_rgm_11pm) * 100 as rgm_c_perc_11pm,
        (moda.moda_disc_11pm/myntra.total_disc_11pm) * 100 as disc_c_perc_11pm
    FROM
        (
            SELECT
                fci.order_created_date as date,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.item_revenue_inc_cashback else 0 END) as total_rev_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.item_revenue_inc_cashback else 0 END) as total_rev_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.rgm_final else 0 END) as total_rgm_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.rgm_final else 0 END) as total_rgm_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.rgm_final else 0 END) as total_rgm_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.rgm_final else 0 END) as total_rgm_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.rgm_final else 0 END) as total_rgm_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.rgm_final else 0 END) as total_rgm_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.rgm_final else 0 END) as total_rgm_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.rgm_final else 0 END) as total_rgm_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.product_discount else 0 END) as total_disc_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.product_discount else 0 END) as total_disc_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.product_discount else 0 END) as total_disc_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.product_discount else 0 END) as total_disc_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.product_discount else 0 END) as total_disc_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.product_discount else 0 END) as total_disc_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.product_discount else 0 END) as total_disc_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.product_discount else 0 END) as total_disc_11pm
            FROM
                fact_core_item fci
            WHERE
                fci.order_created_date BETWEEN [START_DATE] and [END_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
            group by
                date
            order by
                date
        ) myntra
    JOIN
        (
            SELECT
                fci.order_created_date as date,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.item_revenue_inc_cashback else 0 END) as moda_rev_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.rgm_final else 0 END) as moda_rgm_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.rgm_final else 0 END) as moda_rgm_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.rgm_final else 0 END) as moda_rgm_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.rgm_final else 0 END) as moda_rgm_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.rgm_final else 0 END) as moda_rgm_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.rgm_final else 0 END) as moda_rgm_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.rgm_final else 0 END) as moda_rgm_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.rgm_final else 0 END) as moda_rgm_11pm,
                sum(CASE WHEN fci.order_created_time <= 040000 then fci.product_discount else 0 END) as moda_disc_04am,
                sum(CASE WHEN fci.order_created_time <= 080000 then fci.product_discount else 0 END) as moda_disc_08am,
                sum(CASE WHEN fci.order_created_time <= 120000 then fci.product_discount else 0 END) as moda_disc_12pm,
                sum(CASE WHEN fci.order_created_time <= 140000 then fci.product_discount else 0 END) as moda_disc_02pm,
                sum(CASE WHEN fci.order_created_time <= 160000 then fci.product_discount else 0 END) as moda_disc_04pm,
                sum(CASE WHEN fci.order_created_time <= 180000 then fci.product_discount else 0 END) as moda_disc_06pm,
                sum(CASE WHEN fci.order_created_time <= 200000 then fci.product_discount else 0 END) as moda_disc_08pm,
                sum(CASE WHEN fci.order_created_time <= 235959 then fci.product_discount else 0 END) as moda_disc_11pm
            from
                fact_core_item fci
            WHERE
                fci.order_created_date BETWEEN [START_DATE] and [END_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
                and fci.brand in [BRAND]
                and fci.article_type = '[ARTICLE_TYPE]'
                and fci.gender = '[GENDER]'
            group by
                date
            order by
                date
        ) moda
    on moda.date=myntra.date
    order by
    moda.date
) metrics
on metrics.date=dd.full_date
where dd.day_of_the_week < 6
order by metrics.date
'''

INTRA_DAY_TREND_TRACKING_QUERY = '''
SELECT
    metrics.*,
    (metrics.rev_c_perc * metrics.rgm_c_perc / metrics.disc_c_perc) as roi,
    dd.day_string,
    dd.day_of_the_week
FROM
    dim_date dd
JOIN
(
    SELECT
        moda.date,
        (moda.moda_rev/myntra.total_rev) * 100 as rev_c_perc,
        (moda.moda_rgm/myntra.total_rgm) * 100 as rgm_c_perc,
        (moda.moda_disc/myntra.total_disc) * 100 as disc_c_perc
    FROM
        (
            SELECT
                fci.order_created_date as date,
                sum(item_revenue - tax_amount) as total_rev,
                sum(item_revenue - tax_amount - (item_purchase_price_inc_tax - vendor_funding + royalty_commission)) as total_rgm,
                SUM(fci.trade_discount) as total_disc
            FROM
                fact_order_live fci
            WHERE
                fci.order_created_date=[START_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
                and fci.order_created_time<=[HOUR]
            group by
                date
            order by
                date
        ) myntra
    JOIN
        (
            SELECT
                fci.order_created_date as date,
                sum(item_revenue - tax_amount) as moda_rev,
                sum(item_revenue - tax_amount - (item_purchase_price_inc_tax - vendor_funding + royalty_commission)) as moda_rgm,
                SUM(fci.trade_discount) as moda_disc
            from
                fact_order_live fci
            WHERE
                fci.order_created_date=[START_DATE]
                and (fci.is_realised=1 or fci.is_shipped=1)
                and fci.brand in [BRAND]
                and fci.article_type ='[ARTICLE_TYPE]'
                and fci.gender='[GENDER]'
                and fci.order_created_time<=[HOUR]
            group by
                date
            order by
                date
        ) moda
    on moda.date=myntra.date
    order by
    moda.date
) metrics
on metrics.date=dd.full_date
--where dd.day_of_the_week < 6
order by metrics.date
'''